(function(){
  $('.btn').click(function () {
    $('.spanbtn').toggleClass('active');
    $('.cha').toggleClass('active');
    $('.cha1').toggleClass('active');
    $('.copy').slideToggle(600);
  })
  $('.main5-bottom').click(function () {
    $('.main6-things').slideToggle(600);
  })
  $(document).scroll(function () {
    console.log($(document).scrollTop())
    if ($(document).scrollTop() > 300) {
      $('.main1 .container').addClass('active')
    } else if ($(document).scrollTop() < 300) {
      $('.main1 .container').removeClass('active')
    }
    if ($(document).scrollTop() > 1498) {
      $('.time').addClass('active')
    } else if ($(document).scrollTop() < 1498) {
      $('.time').removeClass('active')
    }
    if ($(document).scrollTop() > 2038) {
      $('.main4 .container').addClass('active')
    } else if ($(document).scrollTop() < 2038) {
      $('.main4 .container').removeClass('active')
    }
  })
  $('.main2 .icon').bind('click', function (e) {
    e.preventDefault();
    $('html,body').animate({
      scrollTop: $('.main3').offset().top
    }, 300);
  });

  $('.gotop').bind('click', function (e) {
    e.preventDefault();
    $('html,body').animate({
      scrollTop: 0
    }, 500);
  });

  $('.no-info').hover(function(){
  $('.color').attr('fill','#000')
  },function(){
    $('.color').attr('fill','#fff')
  })
  $(document).ready(function(){
    $('.main .container').addClass('active');
    $('.main1 .container').addClass('active')
  })
  window.onload = function () {
    var mySwiper = new Swiper('.swiper-container', {
      // direction: 'vertical', // 垂直切换选项
      loop: true, // 循环模式选项
      // slidesPerView : 3,  
      // effect: 'cube',
      // effect : 'coverflow',
      autoplay: {
        delay: 2000,
        disableOnInteraction: false,
      },
      speed: 1000,
      // 如果需要分页器
      pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
        clickable: true,
        // type:'progressbar',
        // progressbarFillClass : 'my-pagination-progressbar',
      },

      // 如果需要前进后退按钮
      // navigation: {
      //   nextEl: '.swiper-button-next',
      //   prevEl: '.swiper-button-prev',
      // },

      // 如果需要滚动条
      // scrollbar: {
      //   el: '.swiper-scrollbar',
      // },
    })
    mySwiper.el.onmouseover = function () {
      mySwiper.autoplay.stop();
    }
    mySwiper.el.onmouseout = function () {
      mySwiper.autoplay.start();
    }
  }
}())